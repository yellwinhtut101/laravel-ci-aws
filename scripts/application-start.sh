#!/bin/bash

# Place any tasks to start your Laravel application here.
# Typically, this would involve starting your web server (e.g., Apache or Nginx) and any application services.
# Example: Restart Apache to start serving the Laravel application

sudo systemctl restart httpd
sudo systemctl start php-fpm
sudo systemctl enable php-fpm

exit 0