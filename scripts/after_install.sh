#!/bin/bash
# Place any tasks to perform after installing your Laravel application here.
# This could include running database migrations, setting up environment configuration files, etc.
# Example: Run Laravel database migrations 

sudo chown -R ec2-user:apache /var/www/
sudo chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;

cd /var/www/html/laravel
composer install
cp .env.example .env
php artisan key:generate

echo '<VirtualHost *:80>
       ServerName _;
       DocumentRoot /var/www/html/laravel/public
<Directory /var/www/html/laravel>
              AllowOverride All
       </Directory>
</VirtualHost>' | sudo tee -a /etc/httpd/conf/httpd.conf

sudo mv /etc/httpd/conf.d/welcome.conf /etc/httpd/conf.d/welcome.conf_backup

exit 0

