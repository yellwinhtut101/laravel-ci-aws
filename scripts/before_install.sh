#!/bin/bash
# Place any tasks to perform before installing your Laravel application here.
# For example, you can install system dependencies, set up environment variables, etc.
# Example: Install required packages (assuming you are using apt package manager)

sudo yum update -y
sudo yum install httpd -y
sudo yum localinstall https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
sudo yum install mysql-community-server -y
sudo amazon-linux-extras list
sudo amazon-linux-extras enable php8.1
sudo yum install php-cli php-xml php-json php-mbstring php-process php-common php-fpm php-zip php-mysqlnd git -y

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/composer
sudo chmod +x /usr/bin/composer

exit 0

