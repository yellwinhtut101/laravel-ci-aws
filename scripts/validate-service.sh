#!/bin/bash

# Place any validation or health checks for your Laravel application here.
# Ensure that your application is running correctly before marking the deployment as successful.
# Example: Check if the Laravel application is accessible

curl -f http://localhost || exit 1

exit 0